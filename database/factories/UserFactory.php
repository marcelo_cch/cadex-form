<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'type_doc' => 'DNI',
            'document' => $this->faker->randomNumber(8, true),
            'personal_email' => $this->faker->safeEmail(),
            'business_email' => $this->faker->safeEmail(),
            'assistant_email' => $this->faker->safeEmail(),
            'relation_type' => $this->faker->word(),
        ];
    }
}
