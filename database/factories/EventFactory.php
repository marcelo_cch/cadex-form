<?php

namespace Database\Factories;

use App\Models\Event;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class EventFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Event::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->words(3, true),
            'description' => $this->faker->text(),
            'banner_url' => 'https://picsum.photos/1000/600',
            'zoom_form_url' => 'https://www.google.com/',
            'notification_email' => $this->faker->safeEmail(),
            'registration_startdate' => Carbon::now(),
            'registration_endate' => Carbon::now()->addDays(5),
        ];
    }
}
