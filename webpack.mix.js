const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

//Guest
mix.js('resources/js/guest/app.js', 'public/js/guest')
    .vue()
    .postCss('resources/css/guest/app.css', 'public/css/guest', [
        require("tailwindcss"),
    ])
    .disableNotifications()

// Admin
mix.js('resources/js/admin/app.js', 'public/js/admin')
    .vue()
    .postCss('resources/css/admin/app.css', 'public/css/admin', [
        require("tailwindcss"),
    ])
    .disableNotifications()
