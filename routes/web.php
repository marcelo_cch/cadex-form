<?php

use Illuminate\Support\Facades\Route;

// Admin
Route::prefix('admin')->group(function () {
    Route::redirect('/', '/admin/login');

    Route::middleware('guest')->group(function () {
        Route::get('login', 'Admin\LoginController@index');
        Route::post('login', 'Admin\LoginController@login');
    });

    Route::middleware('auth')->group(function () {
        Route::get('logout', 'Admin\LoginController@logout');

        Route::get('dashboard', 'Admin\EventController@index');
        Route::prefix('events')->group(function () {
            Route::get('/', 'Admin\EventController@getAll');
            Route::post('/', 'Admin\EventController@store');
            Route::get('{event}', 'Admin\EventController@show');
            Route::put('{event}', 'Admin\EventController@update');
            Route::put('{event}/active', 'Admin\EventController@updateActive');
            Route::get('{event}/export_records', 'Admin\EventController@exportRecords');
        });
    });
});

// Guest
Route::redirect('/', '/registro');

Route::middleware(['registrationAvailable'])->group(function () {

    Route::get('registro', 'Guest\RegistrationController@index');
    Route::get('event/active', 'Guest\RegistrationController@eventActive');
    Route::post('event/{event}/registration', 'Guest\RegistrationController@registerParticipant');
});
