// Jquery
window.$ = window.jQuery = require('jquery');

// Bootstrap
import 'bootstrap';
import * as bootstrap from 'bootstrap';
window.bootstrap = bootstrap;

// Datatables
import 'datatables.net-bs5';
import 'datatables.net-responsive-bs5';

// jQuery Validate
import 'jquery-validation';

// jQuery datetime-picker
import 'jquery-datetimepicker/build/jquery.datetimepicker.full.min';
