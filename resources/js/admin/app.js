require('./bootstrap');

// Lang de datatable
window.dt_translation_es = require('./datatableTranslation').default;

// Metodos adicionales para jQuery validate
import register from './additionalMethods';
register(jQuery);

// Configuración de jQuery para usar ajax
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

