export default function register ($) {
    $.extend($.validator.messages, {
        required: "Este campo es requerido",
        remote: "Please fix this field.",
        email: "Por favor ingresa un correo válido",
        url: "Ingresar formato de url incluyendo http o https",
        date: "Por favor ingresa una fecha valida",
        dateISO: "Please enter a valid date (ISO).",
        number: "Por favor ingresa un número válido",
        digits: "Por favor ingrese solo dígitos",
        creditcard: "Please enter a valid credit card number.",
        equalTo: "Los campos no coinciden",
        accept: "Please enter a value with a valid extension.",
        maxlength: $.validator.format("Por favor ingrese como máximo {0} caracteres."),
        minlength: $.validator.format("Por favor ingrese como mínimo {0} caracteres."),
        rangelength: $.validator.format("Please enter a value between {0} and {1} characters long."),
        range: $.validator.format("Please enter a value between {0} and {1}."),
        max: $.validator.format("Por favor ingrese un valor menor o igual a {0}."),
        min: $.validator.format("Por favor ingrese un valor mayor o igual a {0}.")
    });

    $.validator.setDefaults({
        errorClass: 'text-danger',
        highlight(element, errorClass) { },
    });

    $.validator.addMethod("maxsize", function (value, element, param) {
        if (this.optional(element)) {
            return true;
        }

        if ($(element).attr("type") === "file") {
            if (element.files && element.files.length) {
                for (var i = 0; i < element.files.length; i++) {
                    if (element.files[i].size > param) {
                        return false;
                    }
                }
            }
        }

        return true;
    }, $.validator.format("El archivo no puede exceder los {0} bytes."));
}
