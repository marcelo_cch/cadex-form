export default {
    "decimal": "",
    "emptyTable": "No hay información",
    "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
    "infoEmpty": "Mostrando 0 de 0 registros",
    "infoFiltered": "(Filtrado de _MAX_ total registros)",
    "infoPostFix": "",
    "thousands": ",",
    "lengthMenu": "Mostrar _MENU_  <i class='iconify' data-icon='sync' id='reloadDatatable'></i>",
    "loadingRecords": "Cargando...",
    "processing": "Procesando...",
    "search": "<i class='icon iconify' data-icon='search'></i>_INPUT_",
    "searchPlaceholder": "Búsqueda",
    "zeroRecords": "Sin resultados encontrados",
    "paginate": {
        "first": "Primero",
        "last": "Ultimo",
        "next": "Siguiente",
        "previous": "Anterior"
    }
};
