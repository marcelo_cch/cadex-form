@extends('guest.layouts.app')
@section('content')
<template>
    <main class="mx-auto h-screen flex max-w-sm md:max-w-full flex-col md:flex-row">
        <form class="p-6 m-auto md:w-1/2 md:p-10 md:max-w-2xl text-lg order-2 md:order-1" @submit.prevent="submit">
            <div class="w-44 mx-auto mb-7 md:ml-0 md:w-36">
                <img src="{{ asset('images/guest/cadex/cadex_logo.png') }}">
            </div>
            <div class="w-full md:px-12">
                <p class="py-2 text-center mb-2 font-bold text-3xl" style="word-break: break-word;">
                    @{{ event.name }}
                </p>
                <p class="py-2 mb-2 text-break" style="word-break: break-word; text-align: justify; white-space: pre-wrap; font-size: .875rem; line-height: 1.25rem">@{{ event.description }}</p>
                <div class="py-2 grid grid-rows-2 grid-cols-3 mb-5">
                    <label class="font-bold justify-self-end mr-3 mb-3" for="document">DNI</label>
                    <input type="text" id="document"
                        class="text-base justify-self-start col-span-2 mb-2 border border-gray-300 rounded-sm w-32 max-w-full px-1"
                        v-model="form.document">
                    <label class="font-bold justify-self-end mr-3 mb-3" for="email">CORREO</label>
                    <input type="text" id="email"
                        class="text-base justify-self-start col-span-2 border border-gray-300 rounded-sm w-48 max-w-full px-1 mb-2"
                        v-model="form.email">
                </div>
                <div v-if="formErrors">
                    <p class="text-red-500 text-sm">
                        <span class="font-bold">Errores encontrados:</span><br>
                        <span v-for="error in formErrors.document"> @{{error}}<br></span>
                        <span v-for="error in formErrors.email"> @{{error}}<br></span>
                    </p>
                </div>
            </div>
            <div class="py-2">
                <button
                    class="block bg-primary text-white rounded-sm w-full h-10 py-1 px-2 md:w-48 md:ml-auto">ENVIAR</button>
            </div>
        </form>
        <div class="flex w-full md:w-1/2 order-1 md:order-2">
            <div class="w-full m-auto py-10 h-full">
                <img class="bg-gray-50 rounded-sm object-contain object-center w-full h-full" :src="event.banner_url">
            </div>
        </div>
    </main>
</template>
@endsection

@push('scripts')
<script>
    const app = new Vue({
        el: '#app',
        data: {
            form: {
                document: '',
                email: '',
            },
            formErrors: null,
            event: {},
        },
        created() {
            axios.get('/event/active').then(res=>res.data).then(res => {
                this.event = res.data;
            });
        },
        methods: {
            submit() {
                this.formErrors = null;
                axios.post(`/event/${this.event.id}/registration`, this.form).then(res=>res.data).then(res => {
                    if (res.success) {
                        alert(res.message);
                        if (res.data.document_valid) {
                            window.open('https://www.ipae.pe', '_self')
                        }
                        else {
                            window.open(this.event.zoom_form_url, '_self');
                        }

                    }
                }).catch((error) => {
                    if(error.response.status = 422){
                        this.formErrors = error.response.data.errors
                    }else{
                        alert(error.response.data.message)
                    }
                });
            },
        },
    });
</script>
@endpush
