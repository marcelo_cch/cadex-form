<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ asset('css/guest/app.css') }}" rel="stylesheet">
    @stack('styles')
    <title>@yield('title', 'CADEX')</title>
</head>

<body>
    <div id="app">
        @yield('content')
    </div>

    <script src="{{ asset('js/guest/app.js') }}"></script>
    @stack('scripts')
</body>

</html>
