<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ asset('css/admin/app.css') }}" rel="stylesheet">
    @stack('styles')

    <title>@yield('title', 'CADEX')</title>
</head>

<body>
    <div id="app">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <a class="navbar-brand" href="/admin/dashboard">CADEX</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav w-100">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="/admin/dashboard">Inicio</a>
                        </li>
                        <li class="nav-item px-lg-2 ms-lg-auto">
                            <a class="nav-link text-danger" href="/admin/logout">Salir</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div>
            @yield('content')
        </div>
    </div>

    <script src="{{ asset('js/admin/app.js') }}"></script>
    @stack('scripts')
</body>

</html>
