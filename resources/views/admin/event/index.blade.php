@extends('admin.layouts.app')
@section('content')
{{-- MODAL --}}
<div class="modal fade" id="eventFormModal" tabindex="-1" aria-labelledby="FormModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form id="eventForm" class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="FormModalLabel">Evento</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="container g-2">
                    <div class="row">
                        <div class="col-12 mb-3">
                            <label for="name" class="mb-1">Nombre:</label>
                            <input type="text" class="form-control form-control-sm" id="name" name="name"
                                autocomplete="off">
                        </div>
                        <div class="col-12 mb-3">
                            <label for="description" class="mb-1">Descripción:</label>
                            <textarea type="text" class="form-control form-control-sm" id="description"
                                name="description" autocomplete="off"></textarea>
                        </div>
                        <div class="col-12 mb-3">
                            <label for="banner" class="mb-1">Banner:</label>
                            <img class="w-100" id="bannerImg">
                            <input type="file" class="form-control form-control-sm mt-2" id="banner" name="banner"
                                autocomplete="off">
                        </div>
                        <div class="col-6 mb-3">
                            <label for="notification_email" class="mb-1">Correo de Notificación:</label>
                            <input type="email" class="form-control form-control-sm" id="notification_email"
                                name="notification_email" autocomplete="off">
                        </div>
                        <div class="col-6 mb-3">
                            <label for="zoom_form_url" class="mb-1">URL del Formulario de ZOOM:</label>
                            <input type="url" class="form-control form-control-sm" id="zoom_form_url"
                                name="zoom_form_url" autocomplete="off">
                        </div>
                        <div class="col-6 mb-3">
                            <label for="registration_startdate" class="mb-1">Fecha de Inicio:</label>
                            <input type="text" class="form-control form-control-sm" id="registration_startdate"
                                name="registration_startdate" autocomplete="off">
                        </div>
                        <div class="col-6">
                            <label for="registration_endate" class="mb-1">Fecha de Fin:</label>
                            <input type="text" class="form-control form-control-sm" id="registration_endate"
                                name="registration_endate" autocomplete="off">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </form>
    </div>
</div>
{{-- /MODAL --}}

{{-- DATATABLE --}}
<main class="p-4">
    <div class="d-flex flex-column mb-2 text-center text-md-start flex-md-row align-items-center">
        <h2 class="lh-1">EVENTOS</h2>
        <button class="fw-normal h6 ms-md-3 lh-1 mt-1 btn btn-secondary" onclick="WORKSPACE.CONTROLLER.create()">Nuevo
            Evento</button>
    </div>
    <table class="w-100 table table-striped table-bordered nowrap" id="table">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Fecha de Creación</th>
                <th>Estado</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
</main>
{{-- /DATATABLE --}}
@endsection

@push('scripts')
<script>
    window.WORKSPACE = {
        STORAGE: {
            table: $('#table'),
            datatable: null,
            modal: $('#eventFormModal'),
            form: $('#eventForm'),
            current_event_id: null,
            validation: {
                validator: null,
                config: {
                    create: {
                        rules: {
                            name: {
                                required: true,
                                maxlength: 255,
                            },
                            description: {
                                required: true,
                            },
                            banner: {
                                required: false,
                                maxsize: 4194304, // 1MB
                            },
                            notification_email: {
                                required: false,
                                email: true,
                                maxlength: 255,
                            },
                            zoom_form_url: {
                                required: false,
                                url: true,
                            },
                            registration_startdate: {
                                required: false,
                                date: true,
                            },
                            registration_endate: {
                                required: false,
                                date: true,
                            },
                        },
                        submitHandler(form) {
                            const formData = new FormData(form);
                            WORKSPACE.CONTROLLER.store(formData);
                        },
                    },
                    edit: {
                        rules: {
                            name: {
                                required: true,
                                maxlength: 255,
                            },
                            description: {
                                required: true,
                            },
                            banner: {
                                required: false,
                                maxsize: 4194304, // 1MB
                            },
                            notification_email: {
                                required: false,
                                email: true,
                                maxlength: 255,
                            },
                            zoom_form_url: {
                                required: false,
                                url: true,
                            },
                            registration_startdate: {
                                required: false,
                                date: true,
                            },
                            registration_endate: {
                                required: false,
                                date: true,
                            },
                        },
                        submitHandler(form) {
                            const formData = new FormData(form);
                            WORKSPACE.CONTROLLER.update(formData);
                        },
                    },
                },
            },
        },
        CONTROLLER: {
            show(data = {}) {
                $('#name').val(data.name);
                $('#description').val(data.description);
                $('#bannerImg').attr('src', data.banner_url);
                $('#notification_email').val(data.notification_email);
                $('#zoom_form_url').val(data.zoom_form_url);
                $('#registration_startdate').val(data.registration_startdate);
                $('#registration_endate').val(data.registration_endate);
            },
            create() {
                WORKSPACE.HELPER.initCreate();
                WORKSPACE.STORAGE.modal.modal('show');
            },
            store(formData) {
                $.ajax({
                    url: `/admin/events`,
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    data: formData,
                }).done(res => {
                    alert(res.message);
                    WORKSPACE.STORAGE.datatable.ajax.reload();
                    WORKSPACE.STORAGE.modal.modal('hide');
                });
            },
            edit(event_id) {
                WORKSPACE.HELPER.initEdit(event_id);
                $.get(`/admin/events/${event_id}`).done(res => {
                    WORKSPACE.CONTROLLER.show(res.data);
                    WORKSPACE.STORAGE.modal.modal('show');
                });
            },
            update(formData) {
                formData.append('_method', 'put');

                $.ajax({
                    url: `/admin/events/${WORKSPACE.STORAGE.current_event_id}`,
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    data: formData,
                }).done(res => {
                    alert(res.message);
                    WORKSPACE.STORAGE.datatable.ajax.reload();
                    WORKSPACE.STORAGE.modal.modal('hide');
                });
            },
            activate(event_id) {
                $.ajax({
                    url: `/admin/events/${event_id}/active`,
                    type: 'PUT',
                    success(res) {
                        alert(res.message);
                        WORKSPACE.STORAGE.datatable.ajax.reload();
                    }
                });
            },
            exportRecords(event_id) {
                window.open(`/admin/events/${event_id}/export_records`, '_blank');
            },
        },
        HELPER: {
            reset() {
                WORKSPACE.STORAGE.current_event_id = null;
                WORKSPACE.STORAGE.validation.validator.resetForm();
                WORKSPACE.STORAGE.form.trigger('reset');
            },
            initCreate() {
                WORKSPACE.STORAGE.validation.validator.destroy();
                WORKSPACE.STORAGE.validation.validator = WORKSPACE.STORAGE.form.validate(WORKSPACE.STORAGE.validation.config.create);
            },
            initEdit(event_id) {
                WORKSPACE.STORAGE.validation.validator.destroy();
                WORKSPACE.STORAGE.validation.validator = WORKSPACE.STORAGE.form.validate(WORKSPACE.STORAGE.validation.config.edit);

                WORKSPACE.STORAGE.current_event_id = event_id;
            },
        },
        INIT() {
            // Reiniciar al cerrar el modal
            WORKSPACE.STORAGE.modal.on('hidden.bs.modal', e => {
                WORKSPACE.HELPER.reset();
            });

            // Crear datetimes
            $('#registration_startdate').datetimepicker();
            $('#registration_endate').datetimepicker();

            // Iniciar datatable
            WORKSPACE.STORAGE.datatable = WORKSPACE.STORAGE.table.DataTable({
                ajax: {
                    method: "GET",
                    url: '/admin/events',
                    dataSrc: "data"
                },
                columns: [
                    { data: "name" },
                    { data: "created_at" },
                    { data: "status_text" },
                    { data: null, render(row) {
                        const UPDATE = `<a class="d-inline-block text-decoration-none text-primary" role="button" onclick="WORKSPACE.CONTROLLER.edit('${row.id}')">Actualizar</a>`;
                        const ACTIVATE = `<a class=" d-inline-block text-decoration-none text-success ms-2" role="button" onclick="WORKSPACE.CONTROLLER.activate('${row.id}')">Activar</a>`;
                        const RECORDS = `<a class=" d-inline-block text-decoration-none text-secondary ms-2" role="button" onclick="WORKSPACE.CONTROLLER.exportRecords('${row.id}')">Registros</a>`;
                        
                        return UPDATE + ACTIVATE + RECORDS;
                    }}
                ],
                language: dt_translation_es,
                searching: true,
                responsive: true,
            });

            // Iniciar validador
            WORKSPACE.STORAGE.validation.validator = WORKSPACE.STORAGE.form.validate();
        },
    };  
    
    $(document).ready(function() {
        // Iniciar vista
        WORKSPACE.INIT();
    });

</script>
@endpush