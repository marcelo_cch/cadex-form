@extends('admin.layouts.auth')
@push('styles')
<style>
    body,
    html,
    #app {
        height: 100%;
    }

    #app {
        display: flex;
        justify-content: center;
        flex-direction: column;
    }
</style>
@endpush
@section('content')
<main class="max-w-lg mx-auto border rounded-lg border-gray-300 flex flex-col items-center">
    <div class="py-2 px-5">
        <div class="max-w-40 w-full mx-auto">
            <img src="{{ asset('images/guest/cadex/cadex_logo.png') }}">
        </div>
        <h3 class="w-full text-center text-2xl font-bold mt-6">Iniciar sesión</h3>
        <form class="max-w-sm mx-auto" @submit.prevent="submit">
            <div>
                <div class="py-2">
                    <label for="username">USUARIO:</label>
                    <input type="text" id="username" class="border border-gray-300 w-full px-1" v-model="form.username">
                </div>
                <div class="py-2">
                    <label for="password">CONTRASEÑA:</label>
                    <input type="password" id="password" class="border border-gray-300 w-full px-1"
                        v-model="form.password">
                </div>
            </div>
            <div v-if="formErrors">
                <p class="text-red-500 text-sm">
                    <span class="font-bold">Errores encontrados:</span><br>
                    <span v-for="error in formErrors.username"> @{{error}}<br></span>
                    <span v-for="error in formErrors.password"> @{{error}}<br></span>
                </p>
            </div>
            <div class="py-2">
                <button class="rounded-lg block ml-auto font-bold bg-yellow-400 text-white py-2 px-4">ENVIAR</button>
            </div>
        </form>
    </div>
</main>
@endsection

@push('scripts')
<script>
    const app = new Vue({
        el: '#app',
        data: {
            form: {
                username: '',
                password: '',
            },
            formErrors: null
        },
        created() {},
        methods: {
            submit() {
                this.formErrors = null;
                axios.post(`/admin/login`, this.form).then(res=>res.data).then(res => {
                    if (res.success) {
                        window.location.reload();
                    }
                    else {
                        alert(res.message);
                    }
                }).catch((error)=>{
                    if(error.response.status = 422){
                        this.formErrors = error.response.data.errors
                    }else{
                        alert(error.response.data.message)
                    }
                });
            },
        },
    });
</script>
@endpush
