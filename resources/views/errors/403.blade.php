@extends('errors.layout')
@section('title', 'NO AUTORIZADO')
@section('message')
{{ $exception->getMessage() ?? 'NO AUTORIZADO' }}
@endsection