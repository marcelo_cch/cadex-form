<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ asset('css/guest/app.css') }}" rel="stylesheet">
    <title>@yield('title')</title>
</head>
<body>
    <div class="flex items-center justify-center bg-gray-100 h-screen">
        <div class="w-72 max-w-full">
            <div class="bg-gray-100 rounded-sm">
                <img src="{{ asset('images/guest/cadex/cadex_logo.png') }}">
            </div>
            <p class="text-center text-lg mt-2">@yield('message')</p>
        </div>
    </div>
</body>
</html>