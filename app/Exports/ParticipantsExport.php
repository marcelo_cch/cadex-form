<?php

namespace App\Exports;

use App\Models\Event;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ParticipantsExport implements FromQuery, WithHeadings
{
    use Exportable;

    private Event $event;

    public function __construct(Event $event)
    {
        $this->event = $event;
    }

    public function query()
    {
        return DB::table('event_participant as ep')
            ->select([
                'u.type_doc',
                'u.document',
                'u.personal_email',
                'u.business_email',
                'u.assistant_email',
                'ep.email',
                'u.relation_type',
            ])
            ->join('users as u', 'ep.user_id', '=', 'u.id')
            ->where('ep.event_id', $this->event->id)
            ->orderBy('u.type_doc');
    }

    public function headings(): array
    {
        return [
            'TIPO DE DOCUMENTO',
            'DOCUMENTO',
            'CORREO PERSONAL',
            'CORREO DE NEGOCIOS',
            'CORREO DE ASISTENTE',
            'CORREO DEL EVENTO',
            'TIPO DE RELACIÓN',
        ];
    }
}
