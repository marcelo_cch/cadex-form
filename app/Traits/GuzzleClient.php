<?php

namespace App\Traits;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

trait GuzzleClient
{
    public function clientRequest($method, $endpoint, $headers, $body = null)
    {
        $client = new Client();
        $request = new Request($method, $endpoint, $headers, isset($body) ? json_encode($body) : null);
        $response = $client->sendRequest($request);
        $response_status = $response->getStatusCode();
        if ($response_status >= 200 && $response_status <= 299) {
            $resContent = $response->getBody()->getContents();
            return [
                'status' => $response_status,
                'success' => true,
                'message' => $response->getReasonPhrase(),
                'data' => json_decode($resContent),
            ];
        } else {

            return [
                'status' => $response_status,
                'success' => false,
                'message' => $response->getReasonPhrase(),
            ];
        }
    }
}
