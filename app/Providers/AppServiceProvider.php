<?php

namespace App\Providers;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Carbon::setlocale('America/Lima');

        Response::macro('success', function ($message, $data = null) {
            return response()->json([
                'success' => true,
                'message' => $message,
                'data' => $data,
            ]);
        });

        Response::macro('fail', function ($message, $data = null) {
            return response()->json([
                'success' => false,
                'message' => $message,
                'data' => $data,
            ]);
        });
    }
}
