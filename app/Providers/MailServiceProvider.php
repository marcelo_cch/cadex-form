<?php

namespace App\Providers;

use App\Services\Misc\Mailing\Mailer;
use App\Services\Misc\Mailing\Mailers\Sendinblue;
use Illuminate\Support\ServiceProvider;

class MailServiceProvider extends ServiceProvider
{
    /**
     * Retorna el Mailer que se utilizará para el envío de correos
     *
     * @return Mailer
     */
    public function getMailableInstance(): Mailer
    {
        return new Sendinblue();
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        // Injecta el Mailer
        $this->app->bind(Mailer::class, function ($app) {
            return $this->getMailableInstance();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
