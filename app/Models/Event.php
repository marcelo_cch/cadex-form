<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Event extends PersonalizedModel
{

    const STATUS_DRAFT = 0;
    const STATUS_IN_PROGRESS = 1;

    public $timestamps = false;

    protected $fillable = [
        'name',
        'description',
        'banner_url',
        'zoom_form_url',
        'notification_email',
        'registration_startdate',
        'registration_endate',
        'status',
    ];

    protected $attributes = [
        'notification_email' => 'cvalladares@ipae.pe',
        'status' => self::STATUS_DRAFT,
    ];

    protected $dates = [
        'registration_startdate',
        'registration_endate',
        'created_at',
        'updated_at',
    ];

    // Relationships
    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, EventParticipant::class);
    }

    // Accessors
    public function getStatusTextAttribute()
    {
        return ($this->status == self::STATUS_IN_PROGRESS)
            ? 'EN CURSO'
            : 'BORRADOR';
    }

    // Scopes
    public function scopeActive($q)
    {
        return $q->where('status', self::STATUS_IN_PROGRESS);
    }
}
