<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable as AuthenticatableTrait;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Admin extends Model implements Authenticatable
{
    use AuthenticatableTrait;

    public $timestamps = false;

    protected $fillable = [
        'name',
        'email',
        'password',
        'remember_token',
    ];
}
