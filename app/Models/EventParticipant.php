<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventParticipant extends Model
{
    public $table = "event_participant";
    public $timestamps = false;

    protected $fillable = [
        'event_id',
        'user_id',
        'email',
    ];
}
