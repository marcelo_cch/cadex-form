<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class User extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'type_doc',
        'document',
        'personal_email',
        'business_email',
        'assistant_email',
        'relation_type',
    ];

    public function events(): BelongsToMany
    {
        return $this->belongsToMany(Event::class, EventParticipant::class);
    }
}
