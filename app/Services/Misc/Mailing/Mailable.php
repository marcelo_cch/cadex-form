<?php

namespace App\Services\Misc\Mailing;

interface Mailable
{
    public function getBody(): array;
}
