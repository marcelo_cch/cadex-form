<?php

namespace App\Services\Misc\Mailing\Mailables;

use App\Services\Misc\Mailing\Mailable;
use App\Models\Event;

class RegistrationSuccessMail implements Mailable
{
    private $event;
    private $document;
    private $email;

    public function __construct(Event $event, string $document, string $email)
    {
        $this->event = $event;
        $this->document = $document;
        $this->email = $email;
    }

    public function getBody(): array
    {
        return [
            'to' => [
                ['name' => 'Usuario', 'email' => $this->event->notification_email],
            ],
            'sender' => [
                'name' => 'Notificación', 'email' => config('services.sendinblue.notification'),
            ],
            'replyTo' => [
                'name' => 'Notificación', 'email' => config('services.sendinblue.notification'),
            ],
            'templateId' => intval(config('services.sendinblue.template')),
            'subject' => "Notificación: Nuevo registro en {$this->event->name}",
            'params' => [
                'event' => $this->event->name,
                'document' => $this->document,
                'email' => $this->email,
            ],
        ];
    }
}
