<?php

namespace App\Services\Misc\Mailing\Mailers;

use App\Services\Misc\Mailing\Mailable;
use App\Services\Misc\Mailing\Mailer;
use App\Traits\GuzzleClient;

class Sendinblue implements Mailer
{
    use GuzzleClient;

    private $api_key;
    private $endpoint;
    private $data;
    private $headers;

    public function __construct()
    {
        $this->api_key = config('services.sendinblue.api_key');
        $this->endpoint = config('services.sendinblue.endpoint');
        $this->headers = [
            'Content-type' => 'application/json',
            'Accept' => 'application/json',
            'Cache-Control' => 'no-cache',
            'api-key' => $this->api_key
        ];
    }

    public function send(Mailable $mailable): bool
    {
        $res = $this->clientRequest('POST', "{$this->endpoint}/smtp/email", $this->headers, $mailable->getBody());
        return !!$res['success'];
    }

    public function getTemplateInfo($template_id)
    {
        $template_result =  $this->clientRequest('GET', "{$this->endpoint}/smtp/templates/{$template_id}", $this->headers);
        return $template_result;
    }
}
