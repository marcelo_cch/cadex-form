<?php

namespace App\Services\Misc\Mailing;

interface Mailer
{
    public function send(Mailable $mailable): bool;
}
