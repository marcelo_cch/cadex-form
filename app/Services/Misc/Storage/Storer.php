<?php

namespace App\Services\Misc\Storage;

use Illuminate\Support\Facades\Storage;

class Storer
{
    public static function saveFile(string $folder, $file): string
    {
        return Storage::url($file->store("public/{$folder}"));
    }
}
