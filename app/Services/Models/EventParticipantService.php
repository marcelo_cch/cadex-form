<?php

namespace App\Services\Models;

use App\Models\EventParticipant;

class EventParticipantService
{
    public function registerParticipant($event_id, $user_id, string $email): void
    {
        $query = EventParticipant::where('event_id', $event_id)->where('user_id', $user_id);

        if ($query->exists()) {
            $query->update(['email' => $email]);
        } else {
            EventParticipant::create([
                'event_id' => $event_id,
                'user_id' => $user_id,
                'email' => $email,
            ]);
        }
    }
}
