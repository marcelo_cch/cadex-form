<?php

namespace App\Services\Models;

use App\Models\Event;
use App\Services\Misc\Storage\Storer;

class EventService
{
    private Storer $storer;

    public function __construct(Storer $storer)
    {
        $this->storer = $storer;
    }

    public function create(array $data): void
    {
        if (optional($data)['banner']) {
            $data['banner_url'] = $this->storer->saveFile('event', $data['banner']);
        }

        Event::create($data);
    }

    public function update($event, array $data): void
    {
        if (optional($data)['banner']) {
            $data['banner_url'] = $this->storer->saveFile('event', $data['banner']);
        }

        $event->fill($data);
        $event->save();
    }

    public function activate($event): void
    {
        Event::where('id', '!=', $event->id)->update(['status' => Event::STATUS_DRAFT]);
        $event->status = Event::STATUS_IN_PROGRESS;
        $event->save();
    }
}
