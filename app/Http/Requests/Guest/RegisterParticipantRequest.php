<?php

namespace App\Http\Requests\Guest;

use App\Http\Requests\FormRequestTemplate;

class RegisterParticipantRequest extends FormRequestTemplate
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'document' => 'required|digits:8',
            'email' => 'required|email',
        ];
    }
}
