<?php

namespace App\Http\Requests\Admin\EventController;

use App\Http\Requests\FormRequestTemplate;

class StoreRequest extends FormRequestTemplate
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:255',
            'description' => 'required|string',
            'banner' => 'nullable|file',
            // 'mimetypes:video/avi,video/mpeg,video/quicktime|max:123 (KB)'
            'notification_email' => 'nullable|string:max:255',
            'zoom_form_url' => 'nullable|string',
            'registration_startdate' => 'nullable|date',
            'registration_endate' => 'nullable|date',
        ];
    }
}
