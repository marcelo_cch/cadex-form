<?php

namespace App\Http\Middleware;

use App\Models\Event;
use Closure;
use Illuminate\Http\Request;

class RegistrationAvailable
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $eventActive = Event::active()->first();

        if (!$eventActive) {
            abort(403, 'No se encontraron eventos activos para el registro');
        };

        if ($eventActive->registration_startdate->isFuture()) {
            abort(403, 'El registro del evento ' . $eventActive->name . ' aún no ha empezado');
        };

        if ($eventActive->registration_endate->isPast()) {
            abort(403, 'El registro del evento ' . $eventActive->name . ' ha terminado');
        };

        return $next($request);
    }
}
