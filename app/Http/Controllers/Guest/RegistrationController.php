<?php

namespace App\Http\Controllers\Guest;

use App\Http\Controllers\Controller;
use App\Http\Requests\Guest\RegisterParticipantRequest;
use App\Services\Misc\Mailing\Mailables\RegistrationSuccessMail;
use App\Services\Misc\Mailing\Mailer;
use App\Services\Models\EventParticipantService;
use App\Models\Event;
use App\Models\User;

class RegistrationController extends Controller
{
    public function index()
    {
        return view('guest.registration');
    }

    public function eventActive()
    {
        $eventActive = Event::select([
            'id',
            'name',
            'description',
            'banner_url',
            'zoom_form_url',
        ])->active()->first();

        return response()->success('Evento obtenido correctamente', $eventActive);
    }

    public function registerParticipant(
        Event $event,
        RegisterParticipantRequest $request,
        Mailer $mailer,
        EventParticipantService $eventParticipantService
    ) {
        $user = User::where('document', $request->document)->first();
        $document_valid = !!$user;

        if ($document_valid) {
            $eventParticipantService->registerParticipant($event->id, $user->id, $request->email);
            $mailer->send(new RegistrationSuccessMail($event, $request->document, $request->email));
        }

        $message = ($document_valid)
            ? "Gracias por registrarte en el evento {$event->name}"
            : "Usted no se encuentra registrado en nuestra base de datos. A continuación, se le redirigirá a un formulario para completar su registro.";

        return response()->success($message, ['document_valid' => $document_valid]);
    }
}
