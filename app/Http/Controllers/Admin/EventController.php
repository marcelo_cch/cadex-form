<?php

namespace App\Http\Controllers\Admin;

use App\Exports\ParticipantsExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\EventController\StoreRequest;
use App\Http\Requests\Admin\EventController\UpdateRequest;
use App\Models\Event;
use App\Services\Models\EventService;
use Maatwebsite\Excel\Facades\Excel;

class EventController extends Controller
{
    public function index()
    {
        return view('admin.event.index');
    }

    public function getAll()
    {
        $events = Event::all();
        $events->append('status_text');
        return response()->success('Se han cargado los eventos correctamente', $events);
    }

    public function show(Event $event)
    {
        return response()->success('Se ha obtenido el evento correctamente', $event);
    }

    public function store(StoreRequest $request, EventService $eventService)
    {
        $eventService->create($request->validated());
        return response()->success('Se ha creado el evento correctamente');
    }

    public function update(Event $event, UpdateRequest $request, EventService $eventService)
    {
        $eventService->update($event, $request->validated());
        return response()->success('Se ha actualizado el evento correctamente');
    }

    public function updateActive(Event $event, EventService $eventService)
    {
        $eventService->activate($event);
        return response()->success('Se ha activado el evento correctamente');
    }

    public function exportRecords(Event $event)
    {
        return Excel::download(new ParticipantsExport($event), "Registros {$event->name}.xlsx");
    }
}
