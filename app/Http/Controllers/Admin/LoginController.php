<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\LoginRequest;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index()
    {
        return view('admin.auth.login');
    }

    public function login(LoginRequest $request)
    {
        $loggedIn = Auth::attempt([
            'email' => $request->username,
            'password' => $request->password,
        ]);

        if (!$loggedIn) {
            return response()->fail('No se pudo iniciar sesión con las credenciales ingresadas');
        }

        return response()->success('Se ha iniciado sesión correctamente');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/admin/login');
    }
}
